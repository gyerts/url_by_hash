Use Python 2.7 for the project to run (compatibility with 3.x - possible, but not checked)

Execute once:
    
    pip install -r requirements.txt
    
    ./manage.py makemigrations url_encoder
    ./manage.py migrate

For each use:

    ./manage.py runserver 0.0.0.0:8000
