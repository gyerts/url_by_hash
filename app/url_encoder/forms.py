from django import forms
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
import re

url_validate = URLValidator()


class Form(forms.Form):
    url = forms.URLField(label='URL')
    hash = forms.CharField(max_length=15, min_length=15, label="HASH", required=False)

    def clean_url(self):
        url_validate(self.cleaned_data['url'])
        return self.cleaned_data['url']

    def clean_hash(self):
        s = self.cleaned_data['hash']

        if not re.match(r"^[a-zA-Z0-9_]*$", s):
            raise ValidationError("hash should contain only: a-z A-Z 0-9 _ symbols")

        return self.cleaned_data['hash']
