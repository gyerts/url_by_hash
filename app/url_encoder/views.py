import string
import random
from django.shortcuts import render
from . import forms
from . import models
from django.db import IntegrityError
from django.core.exceptions import ValidationError


def get_unique_hash():
    def random_string(length):
        pool = string.letters + string.digits
        return ''.join(random.choice(pool) for i in xrange(length))

    hash = random_string(15)
    while models.Link.objects.filter(hash=hash).exists():
        hash = random_string(15)
    return hash


def url_encoder(request):
    context = {'form': forms.Form}

    if request.method == 'POST':
        form = forms.Form(request.POST)
        if form.is_valid():
            url = form.cleaned_data['url']
            hash = form.cleaned_data['hash']

            try:
                if hash and len(hash) != 15:
                    raise ValidationError(
                        "hash size should be equal 15! Size of provided hash is {}".format(len(hash)))
                if not hash:
                    hash = get_unique_hash()

                link = models.Link(url=url, hash=hash)
                link.save()
            except IntegrityError:
                form.errors['hash'] = ["link with the same hash already exists!"]
                context["form"] = form
            except ValidationError as ex:
                form.errors['hash'] = [str(ex.message)]
                context["form"] = form
            except Exception as ex:
                form.errors['hash'] = [str(ex.message)]
                context["form"] = form

            context["url"] = url
            context["hash"] = hash
            context["generated_url"] = "/hash/get_link/" + hash
        else:
            context["form"] = form

    return render(request, 'url_encoder/form.html', context)


def get_link(request, hash):
    try:
        link = models.Link.objects.get(hash=hash)
        return render(request, 'url_encoder/get_link.html', {
            "url": link.url,
            "hash": link.hash
        })
    except:
        return render(request, '404.html', {})

