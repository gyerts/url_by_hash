from django.conf.urls import url, include

from . import views

alphanumeric_pattern = r'^$'

urlpatterns = [
    url(r'^get_link/(?P<hash>[a-zA-Z0-9_]*)', views.get_link),
    url(r'', views.url_encoder),
]
