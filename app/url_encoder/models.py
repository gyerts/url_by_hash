import re
from django.db import models
from django.contrib import admin
from django.core.validators import RegexValidator
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError


alphanumeric = RegexValidator(r'^[0-9a-zA-Z_]*$', 'Only alphanumeric characters and _ are allowed.')


class Link(models.Model):
    url = models.URLField()
    hash = models.CharField(max_length=15, validators=[alphanumeric], unique=True)

    def __str__(self):
        return "{}: {}".format(self.url, self.hash)


@receiver(pre_save, sender=Link)
def save_user_profile(sender, instance, **kwargs):
    if not re.match(r"^[a-zA-Z0-9_]*$", instance.hash):
        raise ValidationError("hash should contain only: a-z A-Z 0-9 _ symbols")


admin.site.register(Link)
